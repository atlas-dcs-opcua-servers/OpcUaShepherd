import os
import sys
import shutil

sys.path.insert(0, 'UaoForQuasar')
import generateClass

generateClass.runGenerator('Shepherd', 'UaoForQuasar', namespace='UaoClientForShepherd')

if not os.path.isdir('ShepherdClient'):
    print("Not copying the files into shepherd client because ShephercClient directory was not found")

shutil.copy(
    os.path.join('UaoForQuasar','generated','Shepherd.h'), 
    os.path.join('ShepherdClient', 'include'))

shutil.copy(
os.path.join('UaoForQuasar','generated','Shepherd.cpp'), 
os.path.join('ShepherdClient', 'src'))