#include <iostream>

#include <Utils.h>
#include <LogIt.h>

#include <ShepherdsBrain.hpp>

void Sheep::notifyRegistration (const std::string& endpointUrl, const std::string& serverUri)
{
   m_endpointUrl = endpointUrl;
   m_serverUri = serverUri;
   m_lastRegistration = std::chrono::system_clock::now();
}

int Sheep::secondsSinceLastRegistration () const
{
   std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
   return std::chrono::duration_cast<std::chrono::seconds>(now - m_lastRegistration).count();
}

ShepherdsBrain::ShepherdsBrain ()
{
    LOG(Log::INF, "Shepherd") << "Shepherds's brain was spawned to life.";
}

void ShepherdsBrain::registerSheep (const std::string& name, const std::string& endpointUrl, const std::string& serverUri)
{
    LOG(Log::TRC, "Shepherd") << "registerSheep insides, sheepName=" << name;
    const std::lock_guard<std::mutex> lock(m_accessLock);
    if (m_herd.find(name) == m_herd.end())
    {
        // first time we see this sheep -- need to add it.
        Sheep newSheep;
        m_herd[name] = newSheep;
    }
    Sheep& sheep = m_herd[name];
    sheep.notifyRegistration(endpointUrl, serverUri);
}

ResolvedSheep ShepherdsBrain::resolveSheep (const std::string& name)
{
    const std::lock_guard<std::mutex> lock(m_accessLock);
    if (m_herd.find(name) == m_herd.end())
    { // no such entry
        ResolvedSheep noSuchSheep; // it is constructed to not have data by default.
        return noSuchSheep;
    }

    ResolvedSheep resolvedSheep;
    Sheep& sheep = m_herd[name];
    resolvedSheep.endpointUrl = sheep.endpointUrl();
    resolvedSheep.serverUri = sheep.serverUri();
    resolvedSheep.secondsSinceLastRegistration = sheep.secondsSinceLastRegistration();
    resolvedSheep.dataPresent = true;
    return resolvedSheep;
}

void ShepherdsBrain::showPaidCommercial ()
{
    const std::lock_guard<std::mutex> lock(m_accessLock);
    std::cout << "sheep registered: " << Quasar::TermColors::ForeGreen() << m_herd.size() << Quasar::TermColors::StyleReset();
    unsigned int howManyHaveRecentRegistration = 0;
    for (auto nameSheepPair : m_herd)
    {
       if (nameSheepPair.second.secondsSinceLastRegistration() <= 60) // piotr: make this configurable? dunno.
       howManyHaveRecentRegistration++;
    }
    std::cout << " (" << Quasar::TermColors::ForeGreen() << howManyHaveRecentRegistration << Quasar::TermColors::StyleReset() << " up-to-date coordinates). ";

}