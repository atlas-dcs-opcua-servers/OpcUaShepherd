#include <map>
#include <chrono>
#include <mutex>
#include <atomic>

class Sheep
{
    public:

        void notifyRegistration (
            const std::string& endpointUrl,
            const std::string& serverUri);

        int secondsSinceLastRegistration () const;
        std::string endpointUrl() const {return m_endpointUrl;}
        std::string serverUri() const {return m_serverUri;}

    private:
        std::string m_endpointUrl;
        std::string m_serverUri;
        std::chrono::time_point<std::chrono::system_clock> m_lastRegistration;
        
        
};

struct ResolvedSheep
{
    ResolvedSheep(): dataPresent(false) {}
    bool dataPresent;
    std::string endpointUrl;
    std::string serverUri;
    int secondsSinceLastRegistration;
};

class ShepherdsBrain
{

public:
    ShepherdsBrain ();

    void registerSheep (const std::string& name, const std::string& endpointUrl, const std::string& serverUri);
    ResolvedSheep resolveSheep (const std::string& name);
    void showPaidCommercial ();

private:
    std::mutex m_accessLock;

    // this is a map from logical name to the current discovery information
    std::map<std::string, Sheep> m_herd;

};